% Discontinuous Galerkin Time Domain solver of an electromagnetic plane wave
% in an isotropic linear dispersionless medium
% Numerical simulation code by Roman Svoboda
% Data visualization code by Dr. Jasmin Smajic
%
% ETH Zuerich, 2023

clear;
clc;
clf;

% CONSTANTS
eps0   = 8.8541878128e-12;
mu0    = 1.25663706212e-6;
eps_r  = 1.0;
mu_r   = 1.0;
eps    = eps0 * eps_r;
mu     = mu0  * mu_r;
Z_w    = sqrt(mu/eps); % impedance
c      = 299792458.0; % speed of light in [m/s]
omega  = (2.0*pi)/(1e-10); % angular frequency (omega) of the incoming wave; was roughly 2pi/3.405e-10 in the movie from Jasmin
E_0    = [0.0 1.0 0.0]; % amplitude of the incoming wave
% nsteps = 100; % number of time steps to be calculated
max_t  = 1.5e-10; % end time of the simulation
curr_t = 0.0; % current time
rate = 7; % video frame rate, if video is generated

intpl_order = 1; % select order of the interpolating functions. Currently only linear is supported
rerun_prepare_data = false; % select if prepare_data should be rerun each time
use_slope_limiter = true; % set to false to disable the slope limiter
generate_video = true; % set to false to run simulation without video

% uncomment below which folder you would like to load mesh data from
% ======================
% mesh_folder = 'original/';
mesh_folder = 'coarse/';
% mesh_folder = 'fine/';

sma_3D_Data_Handling_Ver_1; % load mesh data

% if preprocessing data exists, load it. Otherwise, compute the preprocessing data
if ~rerun_prepare_data && isfile(strcat(mesh_folder,'el_2d_all_ngh.txt')) && isfile(strcat(mesh_folder,'el_2d_all_no.txt')) && ...
    isfile(strcat(mesh_folder,'el_2d_all_norm.txt')) && isfile(strcat(mesh_folder,'el_3d_faces.txt')) && ...
    isfile(strcat(mesh_folder,'el_2d_all_vol.txt')) && isfile(strcat(mesh_folder,'vert_redundancy.txt')) && ...
    isfile(strcat(mesh_folder,'vert_adjacent_elements.txt'))
    load(strcat(mesh_folder, 'el_2d_all_ngh.txt'));
    Nfcs = size(el_2d_all_ngh,1);
    el_2d_all_ngh = reshape(el_2d_all_ngh, Nfcs, 2, 4);
    load(strcat(mesh_folder, 'el_2d_all_no.txt'));
    load(strcat(mesh_folder, 'el_2d_all_norm.txt'));
    load(strcat(mesh_folder, 'el_3d_faces.txt'));
    el_3d_faces = reshape(el_3d_faces, Ne_3d, 4, 3);
    load(strcat(mesh_folder, 'el_2d_all_vol.txt'));
    load(strcat(mesh_folder, 'vert_redundancy.txt'));
    load(strcat(mesh_folder, 'vert_adjacent_elements.txt'));
else
    rs_prepare_data;
end

rs_matrix_assembly; % generate the element matrices needed in the time-stepping scheme
rs_setup_solution_vars; % computes some mesh-dependent model parameters

if generate_video
    fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
end

disp(['Total time steps: ', num2str(floor(max_t/dt))]);
for step=1:floor(max_t/dt)
    rs_model_step;
    curr_t = curr_t + dt;
    disp(step);
    if generate_video
        Ey = get_res_field(E_all, vert_redundancy, Ne_3d, Nn, el_3d_no);
        sma_results_visualization;
        Frames(step) = getframe(fig1);
    end % if generate_video
end % step=1:nsteps

if generate_video
    flen = length(Frames);
    for K = 1 : flen
        if isempty(Frames(K).cdata)
            all_valid = false;
            disp(['Empty frame occurred at frame ',num2str(K),' of ',num2str(flen)]);
        end
    end
    writerObj = VideoWriter('EM_wave_prop.avi','Motion JPEG AVI');
    writerObj.FrameRate=rate;
    open(writerObj);
    writeVideo(writerObj,Frames);
    close(writerObj);
    generate_video = false;
else
    Ey = get_res_field(E_all, vert_redundancy, Ne_3d, Nn, el_3d_no);
end

sma_results_visualization;

function Ey = get_res_field(E_all, vert_redundancy, Ne_3d, Nn, el_3d_no)
    Ey = zeros(Nn,1);
    for el=1:Ne_3d
        for vert=1:4
            Ey(el_3d_no(el,vert)) = Ey(el_3d_no(el,vert)) + E_all(el,3*vert-1);
        end % vert=1:4
    end % el=1:Ne_3d

    Ey = Ey ./ vert_redundancy;
end