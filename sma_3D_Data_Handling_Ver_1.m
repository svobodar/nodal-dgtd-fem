% Basic Package, 3-D data handling
% March 14, 2021

sma_load_3d_data_comsol_Ver_2;

% return;

% Model Specific Data
% *******************
Px1=70;Px2=900;Py1=70;Py2=900;
visualize_model_data=1;


sma_set_status_1d_elements;
sma_set_status_2d_elements;
sma_set_status_3d_elements;

if visualize_model_data
    vis_model_data_2;
    % return;
end