% Set the status of 3-d elements (model specific)

el_3d_status=zeros(Ne_3d,1);
bou_edges_tets=zeros(Ne_3d,3);
bou_faces_tets=zeros(Ne_3d,1);

for i=1:Ne_3d
    in1=el_3d_no(i,1);in2=el_3d_no(i,2);in3=el_3d_no(i,3);in4=el_3d_no(i,4);
    x1=x_no(in1);x2=x_no(in2);x3=x_no(in3);x4=x_no(in4);
    y1=y_no(in1);y2=y_no(in2);y3=y_no(in3);y4=y_no(in4);
    z1=z_no(in1);z2=z_no(in2);z3=z_no(in3);z4=z_no(in4);
    

    % if el_mat(i)==1
        el_3d_status(i)=1;
    % end
    
end
