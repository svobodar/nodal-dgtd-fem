% E-field step
for el=1:Ne_3d
    increment = zeros(12,1);
    increment = increment + 2.0 .* squeeze(A1_all(el,:,:)) * H_all(el,:)';

    for f=1:4
        increment = increment - squeeze(B1ee_all(el,f,:,:)) * H_all(el,:)';
        face = el_3d_faces(el,f,:);
        
        switch face(3)
            case 0
                increment = increment + squeeze(B1ep_all(el,f,:,:)) * H_all(el_2d_all_ngh(face(1),3-face(2),1),:)';
            case {1, 6} % the faces with x constant
                % do nothing, because boundary conditions require this term to be zero
            case {2, 5} % the faces with y constant
                increment = increment + squeeze(B1ee_all(el,f,:,:)) * H_all(el,:)';
            case 3 % input port
                increment = increment + (1.0/Z_w) .* (squeeze(B1ep_all(el,f,:,:)) * E_all(el,:)');
                increment = increment - (2.0*sin(omega*curr_t)/Z_w) .* (squeeze(B1ep_all(el,f,:,:)) * (source_vec(f,:) .* repmat(E_0,1,4))');
            case 4 % output port
                increment = increment + (1.0/Z_w) .* (squeeze(B1ep_all(el,f,:,:)) * E_all(el,:)');
        end % switch face(3)
    end % f=1:4
    increment = squeeze(A23_all_inv(el,:,:)) * increment;
    increment = (0.5*dt/eps) .* increment;
    
    % the big moment
    E_all(el,:) = E_all(el,:)' + increment;
end % el=1:Ne_3d

if use_slope_limiter
    E_all = slope_limiter(E_all, Ne_3d, el_3d_no, vert_redundancy, vert_adjacent_elements);
end % if use_slope_limiter

% H-field step
for el=1:Ne_3d
    increment = zeros(12,1);
    increment = increment + 2.0 .* squeeze(A1_all(el,:,:)) * E_all(el,:)';
    
    for f=1:4
        increment = increment - squeeze(B1ee_all(el,f,:,:)) * E_all(el,:)';
        face = el_3d_faces(el,f,:);
        
        switch face(3)
            case 0
                increment = increment + squeeze(B1ep_all(el,f,:,:)) * E_all(el_2d_all_ngh(face(1),3-face(2),1),:)';
            case {1, 6} % the faces with x constant
                increment = increment + squeeze(B1ee_all(el,f,:,:)) * E_all(el,:)';
            case {2, 5} % the faces with y constant
                % do nothing, because boundary conditions require this term to be zero
            case 3 % input port
                increment = increment - Z_w .* (squeeze(B1ep_all(el,f,:,:)) * H_all(el,:)');
                increment = increment + (2.0*sin(omega*(curr_t+0.5*dt))) .* (squeeze(B1ee_all(el,f,:,:)) * (source_vec(f,:) .* repmat(E_0,1,4))');
            case 4 % output port
                increment = increment - Z_w .* (squeeze(B1ep_all(el,f,:,:)) * H_all(el,:)');
        end % switch face(3)
    end % f=1:4
    increment = squeeze(A23_all_inv(el,:,:)) * increment;
    increment = (0.5*dt/mu) .* increment;
    
    % the big moment
    H_all(el,:) = H_all(el,:)' - increment;
end % el=1:Ne_3d

if use_slope_limiter
    H_all = slope_limiter(H_all, Ne_3d, el_3d_no, vert_redundancy, vert_adjacent_elements);
end % if use_slope_limiter


% Slope limiter function
% IN: field after a time-step
% OUT: IN-field with slope limiter applied
% Underlying theory here (section 4.2): https://arxiv.org/pdf/2008.11981.pdf
function res_field = slope_limiter(in_field, Ne_3d, el_3d_no, vert_redundancy, vert_adjacent_elements)
    res_field = in_field;
    cell_avgs = zeros(Ne_3d,3); % average E-field for each cell
    for el=1:Ne_3d
        cell_avgs(el,1) = 0.25*sum(in_field(el,1:3:10));
        cell_avgs(el,2) = 0.25*sum(in_field(el,2:3:11));
        cell_avgs(el,3) = 0.25*sum(in_field(el,3:3:12));
    end % for el=1:Ne_3d
    
    for el=1:Ne_3d
        limiting_coeff = [1.0 1.0 1.0];
        for vert=1:4
            E_max = cell_avgs(el,:);
            E_min = cell_avgs(el,:);
            % find the minimal and maximal E-field of a cell sharing this vertex
            for coord=1:3 % loop over xyz
                for adjacent_el_idx=1:vert_redundancy(el_3d_no(el,vert))
                    adjacent_el=vert_adjacent_elements(el_3d_no(el,vert),adjacent_el_idx);
                    if cell_avgs(adjacent_el,coord)>E_max(coord), E_max(coord)=cell_avgs(adjacent_el,coord);end
                    if cell_avgs(adjacent_el,coord)<E_min(coord), E_min(coord)=cell_avgs(adjacent_el,coord);end
                end % for adjacent_el=1:vert_redundancy(el_3d_no(el,vert))
                
                if in_field(el,3*(vert-1)+coord)<E_min(coord)
                    limiting_coeff(coord)=min(limiting_coeff(coord),(E_min(coord)-cell_avgs(el,coord))/(in_field(el,3*(vert-1)+coord)-cell_avgs(el,coord)));
                end
                if in_field(el,3*(vert-1)+coord)>E_max(coord)
                    limiting_coeff(coord)=min(limiting_coeff(coord),(E_max(coord)-cell_avgs(el,coord))/(in_field(el,3*(vert-1)+coord)-cell_avgs(el,coord)));
                end
            end % for coord=1:3
        end % for vert=1:4
        
        for coord=1:3
            if limiting_coeff(coord) < 1.0
                res_field(el,coord:3:(9+coord)) = cell_avgs(el,coord) + limiting_coeff(coord)*(in_field(el,coord:3:(9+coord))-cell_avgs(el,coord));
            end
        end % for coord=1:3
    end % for el=1:Ne_3d
end