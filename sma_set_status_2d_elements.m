% Set the status of 1-d elements (model specific)

el_2d_status=zeros(Ne_2d,1);
for i=1:Ne_2d
    
    in1=el_2d_no(i,1);in2=el_2d_no(i,2);in3=el_2d_no(i,3);
    x1=x_no(in1);x2=x_no(in2);x3=x_no(in3);
    y1=y_no(in1);y2=y_no(in2);y3=y_no(in3);
    z1=z_no(in1);z2=z_no(in2);z3=z_no(in3);
    
    el_2d_status(i)=1;
        
end