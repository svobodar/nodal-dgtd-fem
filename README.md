## Nodal Discontinuous Galerkin solver

This project is a MATLAB implementation of a Nodal Discontinuous Galerkin solver for an electromagnetic plane wave. This implementation only allows for a y-polarized wave propagating in the z direction, in a medium that is homogeneous, linear, isotropic and dispersionless. Any of these assumptions might be relaxed in future implementations, if the project is continued. The Program can be started by running ``rs_dgtd_fem_main.m``. In this file, some model parameters can also be adjusted.

The project was created as part of my Electromagnetics seminar, which I completed during my CSE MSc. programme at ETH Zuerich.

### Theory
The solver works with a tetrahedral mesh. The electric and magnetic field in each element is described by 12 coefficients, which are the three field directions in each of the four vertices. For each vertex, the coefficients are then multiplied by a linear scalar shape function.

The time discretization is made using a leap-frog scheme, i.e. the step from E<sub>t</sub> to E<sub>t+1</sub> is calculated using H<sub>t+1/2</sub> and vice versa.

The domain is chosen as a rectangular cuboid aligned with the three carthesian directions. The faces with constant z are chosen as the input and output ports, so an absorbing boundary condition is implemented there. For the other faces, the boundary condition is chosen to correspond to a propagating plane wave.

### Slope limiter

The basic Discontinuous Galerkin scheme proved to be unstable for this problem. It would generate oscillatory solutions and diverge towards infinity. For this reason, I implemented a slope limiter as part of the time-step function. The slope limiter makes sure, that the field in a vertex is bounded by the minimum and maximum average field in the neighboring cells. This way the gradient stays bounded and the stability of the solution improves significantly.

### Mesh data

The mesh data is contained in the folders ``coarse``, ``fine`` and ``original``. The first two describe the same domain with different resolutions, the last one describes a different one. The original mesh-data was generated from COMSOL, but for this model, an additional pre-processing stage is needed. The pre-processing is done in ``rs_prepare_data.m``. However, since the pre-processing stage is very time-consuming, it must only be run once, and the results are exported into the folder with the COMSOL data. For the three meshes that I worked on, the pre-processing data is already generated in this repo.