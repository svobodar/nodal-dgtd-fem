% Set a 3D figure
% view(ax1,[1,1,1]);
view(ax1,[-0.4,0.7,-0.7]);
hold(ax1,'all');
xlabel('x (m)');
ylabel('y (m)');
zlabel('z (m)');
axis(ax1,'equal');
axis(ax1,[-0.06,0.1,-0.08,0.01,-0.06,0.1]);

