% Visualisation of the model data

plot_bou_edges=0;
plot_bou_tri=1;
plot_tets=0;
plot_edges=0;
plot_nodes=0;
plot_dirichlet_nodes=0;
plot_neumann_nodes=0;
plot_cn_nodes=0;
plot_cn_tri=0; % CN triangles
plot_cn_tri_normals=0;
plot_n_tri=0; % Neumann triangles
plot_n_tri_normals=0;
plot_d_tri=0; % Dirichlet triangles
plot_d_tri_normals=0;

axis_Position=[0.06,0.06,0.95,0.88];

if plot_edges
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
plot_edges_3d;
end

if plot_tets
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
plot_tetrahedrons;
end

if plot_bou_edges
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
end

if plot_bou_tri
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_triangles_3d_Ver_2;
plot_boundary_edges_3d;
end

if plot_nodes
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_nodes_3d_ver_3;
plot_boundary_edges_3d;
title('Nodes (red-CN, black-cyan-green-Dirichlet, blue-Neumann)');
end

if plot_dirichlet_nodes
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_Dirichlet_nodes_3d_ver_1;
plot_boundary_edges_3d;
title('Nodes (Dirichlet_1-red, Dirichlet_2-green, Dirichlet_3-blue)');
end

if plot_neumann_nodes
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_Neumann_nodes_3d_ver_1;
plot_boundary_edges_3d;
title('Neumann Nodes');
end

if plot_cn_nodes
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2],'renderer','zbuffer');
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_CN_nodes_3d_ver_1;
plot_boundary_edges_3d;
title('CN Nodes');
end

if plot_cn_tri
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
plot_cn_triangles_3d_Ver_2;
if plot_cn_tri_normals
    FS=CN_face;
    plot_face_normals_3d; 
end
title('CN-triangles');
end

if plot_n_tri
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
plot_n_triangles_3d_Ver_1;
if plot_n_tri_normals
    FS=Neumann_face;
    plot_face_normals_3d; 
end
title('Neumann-triangles');
end

if plot_d_tri
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
ax1=axes('Parent',fig1,'FontSize',14,'Position',axis_Position);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
plot_dirichlet_triangles_3d_Ver_2;
if plot_d_tri_normals
    FS=Dirichlet_face;
    plot_face_normals_3d; 
end
title('Dirichlet-triangles');
end
