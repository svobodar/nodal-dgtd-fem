% plot field over 3-D slice elements
% boundary triangles (elements_2d.txt)

colormap('jet');

clear cb_tick;
clear cb_values;

cmap=colormap;
[n m]=size(cmap);

Nl=20; % number of field lines equal to the number of colors in palette
delta_l=int8(n/Nl-rem(n,Nl)/Nl);
for i=1:Nl
    cmap1(i,:)=cmap((i-1)*delta_l+1,:);
end

delta_field=(max_field-min_field)/(Nl-1);
for i=1:Nl
   field_lines(i)=min_field+(i-1)*delta_field; 
end

% Colorbar levels
Ncb_level=6;
delta_tick=n/(Ncb_level-1);
delta_cb=(max_field-min_field)/(Ncb_level-1);
for i=1:Ncb_level
    cb_tick(i)=1+int8((i-1)*delta_tick);
    cb_values(i)=min_field+(i-1)*delta_cb;
end
cb_tick(1)=2;
cb_tick(Ncb_level)=n;
% colorbar('East','YTick',cb_tick,'YTickLabel',cb_values,'Color',[0 0 0],'FontSize',14,...
% 'Position',[0.94,0.1,0.05,0.75]);
colorbar('East','YTick',cb_values,'Color',[0 0 0],'FontSize',14,...
'Position',[cbp1,cbp2,cbp3,cbp4]);

% in1=el_2d_no(1,1);in2=el_2d_no(1,2);in3=el_2d_no(1,3);
% x1=x_no(in1);x2=x_no(in2);x3=x_no(in3);
% y1=y_no(in1);y2=y_no(in2);y3=y_no(in3);
% z1=z_no(in1);z2=z_no(in2);z3=z_no(in3);
x1=0;x2=0.0001;x3=0;
y1=0;y2=0;y3=0.0001;
z1=0;z2=0;z3=0;
fill3([x1,x2,x3]',[y1,y2,y3]',[z1,z2,z3]',[max_field,min_field,min_field]','EdgeColor','none');  

for i=1:Ne_2d % Loop over boundary triangular elements
    if el_2d_status(i)==1

        in1=el_2d_no(i,1);in2=el_2d_no(i,2);in3=el_2d_no(i,3);
        x1=x_no(in1);x2=x_no(in2);x3=x_no(in3);
        y1=y_no(in1);y2=y_no(in2);y3=y_no(in3);
        z1=z_no(in1);z2=z_no(in2);z3=z_no(in3);

        V1=V(in1);V2=V(in2);V3=V(in3);
        if V1>max_field
           V1=max_field; 
        end
        if V2>max_field
           V2=max_field; 
        end
        if V3>max_field
           V3=max_field; 
        end

        if V1<min_field
           V1=min_field; 
        end
        if V2<min_field
           V2=min_field; 
        end
        if V3<min_field
           V3=min_field; 
        end
        
        fill3([x1,x2,x3]',[y1,y2,y3]',[z1,z2,z3]',[V1,V2,V3]','EdgeColor','none');  
    end
end

% cb=colorbar('East','Color',[0 0 0],'FontSize',14,'Position',[0.9,0.1,0.05,0.7]);
