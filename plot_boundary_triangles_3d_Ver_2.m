% plot PEC elements
% boundary triangles (elements_2d.txt)
face_color=[0.3,0.7,1];
edge_color=[0.2,0.2,0.2];
for i=1:Ne_2d % Loop over boundary triangular elements
    % if el_mat_2d(i)==1
    if el_2d_status(i)==1
        in1=el_2d_no(i,1);in2=el_2d_no(i,2);in3=el_2d_no(i,3);
        x1=x_no(in1);x2=x_no(in2);x3=x_no(in3);
        y1=y_no(in1);y2=y_no(in2);y3=y_no(in3);
        z1=z_no(in1);z2=z_no(in2);z3=z_no(in3);
        fill3([x1,x2,x3]',[y1,y2,y3]',[z1,z2,z3]',face_color,'EdgeColor',edge_color);  
    end
end
