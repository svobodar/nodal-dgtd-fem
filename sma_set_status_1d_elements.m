% Set the status of 1-d elements (model specific)

el_1d_status=zeros(Ne_1d,1);
for i=1:Ne_1d
    
    in1=el_1d_no(i,1);in2=el_1d_no(i,2);
    x1=x_no(in1);x2=x_no(in2);
    y1=y_no(in1);y2=y_no(in2);
    z1=z_no(in1);z2=z_no(in2);

    el_1d_status(i)=1;
      
end