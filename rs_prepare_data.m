% Data preprocessing function

Nfcs = 0.5*(4*Ne_3d + Ne_2d); % number of distint triangles in the mesh
el_2d_all_no = zeros(Nfcs,3); % array of all triangular faces in the mesh (not just boundary)
el_2d_all_ngh = zeros(Nfcs,2,4); % array of the neighboring tetrahedra to the triangular elements.
el_2d_all_norm = zeros(Nfcs,3); %array of normal vectors to the element faces
% el_2d_all_norm (i) will face outward from the element el_2d_all_ngh(j,1,1)
% and inward to the element el_2d_all_ngh(i,1,1)
el_3d_faces = zeros(Ne_3d,4,3); % array containing the indices of the four faces for each 3d element
% el_3d_faces(:,:,2) contains the border domain of the face, or 0 if not on a border
el_2d_all_vol = zeros(Nfcs,1); % area of the triangular faces

vert_redundancy = zeros(Nn,1); % contains number of elements that each vertex is a part of.
for el=1:Ne_3d
    vert_redundancy(el_3d_no(el,:)) = vert_redundancy(el_3d_no(el,:)) + 1;
end
max_elements_per_vert = max(vert_redundancy);

vert_adjacent_elements = zeros(Nn,max_elements_per_vert); % indices of all 3d elements that contain the vertex

curr_row = 1;
tot_faces = 0;
for i=1:Ne_3d
    if mod(i,100) == 0, disp(i); end

    % update vert_adjacent_elements
    for vert=1:4
        vert_adjacent_elements(el_3d_no(i,vert),find(vert_adjacent_elements(el_3d_no(i,vert),:)==0,1)) = i;
    end

    f1 = true;
    f2 = true;
    f3 = true;
    f4 = true;
    
    for j=tot_faces:-1:1
        v1 = find(el_2d_all_no(j,:)==el_3d_no(i,1),1);
        v2 = find(el_2d_all_no(j,:)==el_3d_no(i,2),1);
        v3 = find(el_2d_all_no(j,:)==el_3d_no(i,3),1);
        v4 = find(el_2d_all_no(j,:)==el_3d_no(i,4),1);
        if ~(isempty(v1) || isempty(v2) || isempty(v3))
            f1 = false;
            newln = zeros(4,1);
            newln (1) = i;
            newln(v1+1) = 1;
            newln(v2+1) = 2;
            newln(v3+1) = 3;
            el_2d_all_ngh(j,2,:) = newln;
            el_3d_faces(i,1,1:2) = [j,2];
            continue;
        end
        if ~(isempty(v2) || isempty(v3) || isempty(v4))
            f2 = false;
            newln = zeros(4,1);
            newln (1) = i;
            newln(v2+1) = 2;
            newln(v3+1) = 3;
            newln(v4+1) = 4;
            el_2d_all_ngh(j,2,:) = newln;
            el_3d_faces(i,2,1:2) = [j,2];
            continue;
        end
        if ~(isempty(v3) || isempty(v4) || isempty(v1))
            f3 = false;
            newln = zeros(4,1);
            newln (1) = i;
            newln(v3+1) = 3;
            newln(v4+1) = 4;
            newln(v1+1) = 1;
            el_2d_all_ngh(j,2,:) = newln;
            el_3d_faces(i,3,1:2) = [j,2];
            continue;
        end
        if ~(isempty(v4) || isempty(v1) || isempty(v2))
            f4 = false;
            newln = zeros(4,1);
            newln (1) = i;
            newln(v4+1) = 4;
            newln(v1+1) = 1;
            newln(v2+1) = 2;
            el_2d_all_ngh(j,2,:) = newln;
            el_3d_faces(i,4,1:2) = [j,2];
        end
        if ~f1 && ~f2 && ~f3 && ~f4, break; end
    end % j=tot_faces:-1:1
    
    if f1
        tot_faces = tot_faces+1;
        el_2d_all_no(tot_faces,:) = [el_3d_no(i,1), el_3d_no(i,2), el_3d_no(i,3)];
        el_2d_all_ngh(tot_faces,1,:) = [i,1,2,3];

        in1=el_3d_no(i,1);in2=el_3d_no(i,2);in3=el_3d_no(i,3);in4=el_3d_no(i,4);
        el_2d_all_norm(tot_faces,:) = findnorm(nodes(in1,:), nodes(in2,:), nodes(in3,:), nodes(in4,:));
        el_3d_faces(i,1,1:2) = [tot_faces,1];
        el_2d_all_vol(tot_faces) = 0.5*norm(cross(nodes(in2,:)-nodes(in1,:),nodes(in3,:)-nodes(in1,:)));
    end
    if f2
        tot_faces = tot_faces+1;
        el_2d_all_no(tot_faces,:) = [el_3d_no(i,2), el_3d_no(i,3), el_3d_no(i,4)];
        el_2d_all_ngh(tot_faces,1,:) = [i,2,3,4];

        in1=el_3d_no(i,1);in2=el_3d_no(i,2);in3=el_3d_no(i,3);in4=el_3d_no(i,4);
        el_2d_all_norm(tot_faces,:) = findnorm(nodes(in2,:), nodes(in3,:), nodes(in4,:), nodes(in1,:));
        el_3d_faces(i,2,1:2) = [tot_faces,1];
        el_2d_all_vol(tot_faces) = 0.5*norm(cross(nodes(in2,:)-nodes(in3,:),nodes(in4,:)-nodes(in3,:)));
    end
    if f3
        tot_faces = tot_faces+1;
        el_2d_all_no(tot_faces,:) = [el_3d_no(i,3), el_3d_no(i,4), el_3d_no(i,1)];
        el_2d_all_ngh(tot_faces,1,:) = [i,3,4,1];

        in1=el_3d_no(i,1);in2=el_3d_no(i,2);in3=el_3d_no(i,3);in4=el_3d_no(i,4);
        el_2d_all_norm(tot_faces,:) = findnorm(nodes(in3,:), nodes(in4,:), nodes(in1,:), nodes(in2,:));
        el_3d_faces(i,3,1:2) = [tot_faces,1];
        el_2d_all_vol(tot_faces) = 0.5*norm(cross(nodes(in1,:)-nodes(in3,:),nodes(in4,:)-nodes(in3,:)));
    end
    if f4
        tot_faces = tot_faces+1;
        el_2d_all_no(tot_faces,:) = [el_3d_no(i,4), el_3d_no(i,1), el_3d_no(i,2)];
        el_2d_all_ngh(tot_faces,1,:) = [i,4,1,2];

        in1=el_3d_no(i,1);in2=el_3d_no(i,2);in3=el_3d_no(i,3);in4=el_3d_no(i,4);
        el_2d_all_norm(tot_faces,:) = findnorm(nodes(in4,:), nodes(in1,:), nodes(in2,:), nodes(in3,:));
        el_3d_faces(i,4,1:2) = [tot_faces,1];
        el_2d_all_vol(tot_faces) = 0.5*norm(cross(nodes(in1,:)-nodes(in2,:),nodes(in4,:)-nodes(in2,:)));
    end
end

% for faces on the border of the domain, identify which face they are on
% Domain numbers identified (old mesh):
% d1: x == -0.01
% d2: y == -0.01
% d3: z == 0
% d4: z == 0.1
% d5: y == 0.01
% d6: x == 0.01
%
% Domain numbers identified (new mesh):
% d1: x == 0
% d2: y == 0
% d3: z == 0
% d4: z == 0.05
% d5: y == 0.01
% d6: x == 0.02

for i=1:size(el_2d_all_no,1)
    if el_2d_all_ngh(i,2,1) ~= 0
        continue;
    end
    v1 = el_2d_all_no(i,1);
    v2 = el_2d_all_no(i,2);
    v3 = el_2d_all_no(i,3);

    for j=1:Ne_2d
        f1 = find(el_2d_no(j,:)==v1,1);
        f2 = find(el_2d_no(j,:)==v2,1);
        f3 = find(el_2d_no(j,:)==v3,1);
        if ~(isempty(f1) || isempty(f2) || isempty(f3))
            el_2d_all_ngh(i,2,2) = domains_2d(j)+1;
            curr_el = el_2d_all_ngh(i,1,1);
            idx = find(~ismember(1:4,el_2d_all_ngh(i,1,2:4)),1);
            el_3d_faces(curr_el,mod(idx,4)+1,3) = domains_2d(j)+1;
            break;
        end
    end
end

% export all the generated arrays to files, so that this script does not have to be rerun
% for every simulation attempt.
writematrix(el_2d_all_ngh, strcat(mesh_folder, 'el_2d_all_ngh.txt'));
writematrix(el_2d_all_no, strcat(mesh_folder, 'el_2d_all_no.txt'));
writematrix(el_2d_all_norm, strcat(mesh_folder, 'el_2d_all_norm.txt'));
writematrix(el_3d_faces, strcat(mesh_folder, 'el_3d_faces.txt'));
writematrix(el_2d_all_vol, strcat(mesh_folder, 'el_2d_all_vol.txt'));
writematrix(vert_redundancy, strcat(mesh_folder, 'vert_redundancy.txt'));
writematrix(vert_adjacent_elements, strcat(mesh_folder, 'vert_adjacent_elements.txt'));


% findnorm
% IN: the 3d coordinates of the 4 vertices of the element on which the face lies.
% The element that's not on the face we are trying to find the normal of is passed as last.
% OUT: the normal vector pointing out of the element.
function n = findnorm(nd1, nd2, nd3, nd4)
    edge1 = nd1 - nd2;
    edge2 = nd1 - nd3;
    cprod = cross(edge1, edge2);
    cprod = cprod ./ norm(cprod);
    if dot(cprod,nd4-nd1) > 0
        n = -cprod;
    else
        n = cprod;
    end
end