A1_all   = zeros(Ne_3d, 12, 12); % A1 matrix for each 3d element
A23_all_inv  = zeros(Ne_3d, 12, 12); % A2/A3 matrix for each 3d element (without mu or epsilon)
B1ep_all = zeros(Ne_3d, 4, 12, 12); % B1_ep matrix for each face of each element
B1ee_all = zeros(Ne_3d, 4, 12, 12); % B1_ee matrix for each face of each element

% A23_model = zeros(12,12); % since all these matrices are the same up to a factor, we can predefine them here
% for i=1:3:10
%     A23_model(i,:)   = [1,0,0,1,0,0,1,0,0,1,0,0];
%     A23_model(i+1,:) = [0,1,0,0,1,0,0,1,0,0,1,0];
%     A23_model(i+2,:) = [0,0,1,0,0,1,0,0,1,0,0,1];
%     A23_model(i:i+2,i:i+2) = 2*A23_modell(i:i+2,i:i+2);
% end % i=1:3:10
% A23_model_orig = A23_model;
A23_model = repmat(eye(3),4) + eye(12);
A23_model = A23_model .* (1.0/20.0);
A23_model = inv(A23_model);

for el=1:Ne_3d
    % vertex coordinates of element 'el'
    v1=nodes(el_3d_no(el,1),:);
    v2=nodes(el_3d_no(el,2),:);
    v3=nodes(el_3d_no(el,3),:);
    v4=nodes(el_3d_no(el,4),:);
    
    % boundary domains of faces of 'el' (zero if not on boundary)
    d = el_3d_faces(el,:,3);
    
    % generate a^e_i, b^e_i, c^e_i, d^e_i
    sc = zeros(4,4); % coefficients of the shape functions
    element_mat = zeros(4,4);
    element_mat(:,1)=[1,v1];element_mat(:,2)=[1,v2];element_mat(:,3)=[1,v3];element_mat(:,4)=[1,v4];
    
    for i=1:4
        for j=1:4
            sc(i,j) = (-1).^(i+j) * det(element_mat(1:4~=i, 1:4~=j));
        end % j=1:4
    end % i=1:4

    % normal vectors to the 4 faces (en=element normals)
    en = zeros(4,3);
    for f=1:4
        en(f,:) = el_2d_all_norm(el_3d_faces(el,f,1),:);
        
        % if the normal vector points inside the element, make it point out of it
        % if el_2d_all_ngh(el_3d_faces(el,f,1),1,1) ~= el
        if el_3d_faces(el,f,2) == 2
            en(f,:) = en(f,:) * (-1.0);
        end
    end

    for i=1:3:10
        % super secret formula for the A1 matrix...
        A1_all(el,i,:)   = [0,-sc(4,1),sc(3,1),0,-sc(4,2),sc(3,2),0,-sc(4,3),sc(3,3),0,-sc(4,4),sc(3,4)];
        A1_all(el,i+1,:) = [sc(4,1),0,-sc(2,1),sc(4,2),0,-sc(2,2),sc(4,3),0,-sc(2,3),sc(4,4),0,-sc(2,4)];
        A1_all(el,i+2,:) = [-sc(3,1),sc(2,1),0,-sc(3,2),sc(2,2),0,-sc(3,3),sc(2,3),0,-sc(3,4),sc(2,4),0];
        
        % super secret formula for the B1ee matrix
        if i ~= 10
            B1ee_all(el,1,i,:)   = [0,-en(1,3),en(1,2),0,-en(1,3),en(1,2),0,-en(1,3),en(1,2),0,0,0];
            B1ee_all(el,1,i+1,:) = [en(1,3),0,-en(1,1),en(1,3),0,-en(1,1),en(1,3),0,-en(1,1),0,0,0];
            B1ee_all(el,1,i+2,:) = [-en(1,2),en(1,1),0,-en(1,2),en(1,1),0,-en(1,2),en(1,1),0,0,0,0];
        end

        if i ~= 1
            B1ee_all(el,2,i,:)   = [0,0,0,0,-en(2,3),en(2,2),0,-en(2,3),en(2,2),0,-en(2,3),en(2,2)];
            B1ee_all(el,2,i+1,:) = [0,0,0,en(2,3),0,-en(2,1),en(2,3),0,-en(2,1),en(2,3),0,-en(2,1)];
            B1ee_all(el,2,i+2,:) = [0,0,0,-en(2,2),en(2,1),0,-en(2,2),en(2,1),0,-en(2,2),en(2,1),0];
        end

        if i ~= 4
            B1ee_all(el,3,i,:)   = [0,-en(3,3),en(3,2),0,0,0,0,-en(3,3),en(3,2),0,-en(3,3),en(3,2)];
            B1ee_all(el,3,i+1,:) = [en(3,3),0,-en(3,1),0,0,0,en(3,3),0,-en(3,1),en(3,3),0,-en(3,1)];
            B1ee_all(el,3,i+2,:) = [-en(3,2),en(3,1),0,0,0,0,-en(3,2),en(3,1),0,-en(3,2),en(3,1),0];
        end

        if i ~= 7
            B1ee_all(el,4,i,:)   = [0,-en(4,3),en(4,2),0,-en(4,3),en(4,2),0,0,0,0,-en(4,3),en(4,2)];
            B1ee_all(el,4,i+1,:) = [en(4,3),0,-en(4,1),en(4,3),0,-en(4,1),0,0,0,en(4,3),0,-en(4,1)];
            B1ee_all(el,4,i+2,:) = [-en(4,2),en(4,1),0,-en(4,2),en(4,1),0,0,0,0,-en(4,2),en(4,1),0];
        end
        
        B1ee_all(el,:,i:i+2,i:i+2) = B1ee_all(el,:,i:i+2,i:i+2)*2; % double the integral for i==j
    end % i=1:3:10
    % A1_all(el,:,:) = A1_all(el,:,:) .* ((1.0/4.0)*Ve(el)); % multiply A1_all by the N_i term
    A1_all(el,:,:) = A1_all(el,:,:) .* ((1.0/24.0)); % multiply A1_all by the N_i term
    A23_all_inv(el,:,:) = A23_model(:,:) ./ Ve(el); % multiply by constant factor
    
    for f=1:4 % loop over the four faces
        % scale the B1ee matrix appropriately
        B1ee_all(el,f,:,:) = B1ee_all(el,f,:,:) .* ((1.0/12.0)*el_2d_all_vol(el_3d_faces(el,f,1)));
        face_id = el_3d_faces(el,f,1);
        % elnf = 1 + (el_2d_all_ngh(face_id,1,1) ~= el);  %to see whether this element comes first or second in el_2d_all_ngh
        elnf = el_3d_faces(el,f,2);
        
        % colculate the B1ep/C1ee matrix
        if el_3d_faces(el,f,3) == 0
            % the following loop shuffles around the matrix, so that the correct corner from the neighboring element gets matched
            % to the correct corner of the current element. At least I hope that it does that.
            for vert=2:4
                B1ep_all(el,f,:,(3*el_2d_all_ngh(face_id,3-elnf,vert)-2):(3*el_2d_all_ngh(face_id,3-elnf,vert))) = ...
                    B1ee_all(el,f,:,(3*el_2d_all_ngh(face_id,elnf,vert)-2):(3*el_2d_all_ngh(face_id,elnf,vert)));
            end % vert=2:4
        elseif el_3d_faces(el,f,3) == 3 || el_3d_faces(el,f,3) == 4
            % for the input and output ports, we need to set B1ep to C1ee. Super secret formula follows
            norm_outer = en(f,:)'*en(f,:);
            assert(size(norm_outer,1) == 3 && size(norm_outer,2) == 3); % make sure that we actually have the outer product
            B1ep_all(el,f,:,:) = (repmat(norm_outer,4) + blkdiag(norm_outer,norm_outer,norm_outer,norm_outer)) ...
                - (repmat(eye(3),4) + eye(12));

            % remove all entries corresponding to the node opposite to face 'f'
            exclude_node = mod(f+2,4)+1;
            B1ep_all(el,f,(3*exclude_node-2):(3*exclude_node),:) = 0;
            B1ep_all(el,f,:,(3*exclude_node-2):(3*exclude_node)) = 0;

            % again, scale appropriately
            B1ep_all(el,f,:,:) = B1ep_all(el,f,:,:) .* ((1.0/12.0)*el_2d_all_vol(el_3d_faces(el,f,1)));
        end %el_3d_faces(f,2) == 0
    end % f=1:4
    
end % el=1:Ne_3d