% Visualize the obtained results
% Axis Position
Apx1=0.02;Apx2=0.9;
Apy1=0.1;Apy2=0.85;
% Colorbar Position
cbp1=0.87;cbp2=0.1;
cbp3=0.05;cbp4=0.8;

if 1
V=Ey;
if ~generate_video
    fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
else
    clf;
end
ax1=axes('Parent',fig1,'FontSize',14,'Position',[Apx1,Apy1,Apx2,Apy2]);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
% set_status_2d_elements_h;
% set_status_2d_elements;
if generate_video
    min_field=-1;max_field=+1;
else
    find_min_max;
end
plot_scalar_field_boundary_triangles_3d_Ver_3;
% set_colorbar;
% title(['Electric Field E_y (A/m) at time ',num2str(time(k)),' (s)']);
% title(['Electric Field E_y (V/m) at time ',num2str(t),' (s)']);
end

if 0
V=H_x;
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
ax1=axes('Parent',fig1,'FontSize',14,'Position',[Apx1,Apy1,Apx2,Apy2]);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
% set_status_2d_elements_h;
% set_status_2d_elements;
find_min_max;
% min_field=0;max_field=10000;
plot_scalar_field_boundary_triangles_3d_Ver_3;
% set_colorbar;
% title(['Magnetic Field H_x (A/m) at time ',num2str(time(k)),' (s)']);
title(['Magnetic Field H_x (A/m) at time ',num2str(t),' (s)']);
end

if 0
V=Hsa*sin(omega*time(k));
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
ax1=axes('Parent',fig1,'FontSize',14);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
% set_status_2d_elements_h;
% set_status_2d_elements;
% find_min_max;
min_field=0;max_field=2000;
cbp1=0.9;cbp2=0.1;cbp3=0.05;cbp4=0.8;
plot_scalar_field_boundary_triangles_3d_Ver_3;
% set_colorbar;
title(['Magnetic Field Hsa (A/m)']);
end

if 0
V=Ja;
fig1=figure('Color',[1,1,1],'Position',[Px1,Py1,Px2,Py2]);
ax1=axes('Parent',fig1,'FontSize',14);
set_figure_1_3d;
grid on;
plot_boundary_edges_3d;
% set_status_2d_elements_h;
% set_status_2d_elements;
find_min_max;
% min_field=0;max_field=1e4;
cbp1=0.9;cbp2=0.1;cbp3=0.05;cbp4=0.8;
plot_scalar_field_boundary_triangles_3d_Ver_3;
% set_colorbar;
title(['Current Density J_a (A/m^2)']);
end

