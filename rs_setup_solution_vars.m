E_all = zeros(Ne_3d,12);
H_all = zeros(Ne_3d,12);

min_edge_len = norm(nodes(1,:)-nodes(2,:));
for i=1:Nfcs
    if norm(nodes(el_2d_all_no(i,1),:)-nodes(el_2d_all_no(i,2),:)) < min_edge_len
        min_edge_len = norm(nodes(el_2d_all_no(i,1),:)-nodes(el_2d_all_no(i,2),:));
    end
    if norm(nodes(el_2d_all_no(i,2),:)-nodes(el_2d_all_no(i,3),:)) < min_edge_len
        min_edge_len = norm(nodes(el_2d_all_no(i,2),:)-nodes(el_2d_all_no(i,3),:));
    end
    if norm(nodes(el_2d_all_no(i,3),:)-nodes(el_2d_all_no(i,1),:)) < min_edge_len
        min_edge_len = norm(nodes(el_2d_all_no(i,3),:)-nodes(el_2d_all_no(i,1),:));
    end
end % i=1:Nfcs

dt = (min_edge_len/3.0) * sqrt(mu_r * eps_r) * (1.0/(intpl_order*intpl_order)) / c;

source_vec = [1 1 1 1 1 1 1 1 1 0 0 0
              0 0 0 1 1 1 1 1 1 1 1 1
              1 1 1 0 0 0 1 1 1 1 1 1
              1 1 1 1 1 1 0 0 0 1 1 1];