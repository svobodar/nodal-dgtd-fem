% plot geometry
% boundary edges (elements_1d.txt)

for i=1:Ne_1d % Loop over boundary line elements
    % if el_mat_1d(i)==1
    if el_1d_status(i)==1
    in1=el_1d_no(i,1);in2=el_1d_no(i,2);
    x1=x_no(in1);x2=x_no(in2);
    y1=y_no(in1);y2=y_no(in2);
    z1=z_no(in1);z2=z_no(in2);
    plot3([x1,x2],[y1,y2],[z1,z2],'Color',[0,0,1],'LineWidth',1.5);  
    end
end
