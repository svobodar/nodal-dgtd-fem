% Load comsol data files
% nodes.txt: contains the xyz coordinates of the individual vertices
% elements_1d.txt: contains the index pairs of vertices that are connected by an edge,
%                  BUT ONLY FOR EDGES THAT COINCIDE WITH THE EDGES OF THE DOMAIN
% elements_2d.txt: contains index triplets of vertices that form a face,
%                  BUT ONLY FOR FACES ON THE BOUNDARY OF THE DOMAIN
% elements_3d.txt: contains index quadruplets of vertices that form a tetrahedral element
% domains_1d.txt: for each edge in elements_1d, contains the index of the domain edge that this 1d element is a part of
% domains_2d.txt: for each face in elements_2d, contains the index of the domain face that this 2d element is a part of
% domains_3d.txt: since all the 3d elements are part of the same domain, this is just 1 for every element,
%                 and can essentially be ignored

load(strcat(mesh_folder, 'nodes.txt'));
load(strcat(mesh_folder, 'elements_1d.txt'));
load(strcat(mesh_folder, 'elements_2d.txt'));
load(strcat(mesh_folder, 'elements_3d.txt'));
load(strcat(mesh_folder, 'domains_3d.txt'));
load(strcat(mesh_folder, 'domains_2d.txt'));
load(strcat(mesh_folder, 'domains_1d.txt'));

[Nn, ~]=size(nodes);
[Ne_1d, ~]=size(elements_1d);
[Ne_2d, ~]=size(elements_2d);
[Ne_3d, ~]=size(elements_3d);

el_1d_no=zeros(Ne_1d,2);
el_2d_no=zeros(Ne_2d,3);
el_3d_no=zeros(Ne_3d,4);

x_no=zeros(Nn,1);
y_no=zeros(Nn,1);
z_no=zeros(Nn,1);
st_no=zeros(Nn,1);
xec=zeros(Ne_3d,1);yec=zeros(Ne_3d,1);zec=zeros(Ne_3d,1);
Ve=zeros(Ne_3d,1);

for i=1:Nn
   x_no(i)=nodes(i,1);
   y_no(i)=nodes(i,2);
   z_no(i)=nodes(i,3);
   st_no(i)=0;
end

for i=1:Ne_1d
   for j=1:2
       el_1d_no(i,j)=elements_1d(i,j)+1;  
   end
end

% return;

for i=1:Ne_2d
   for j=1:3
       el_2d_no(i,j)=elements_2d(i,j)+1;  
   end
end

for i=1:Ne_3d
   for j=1:4
       el_3d_no(i,j)=elements_3d(i,j)+1;  
   end

   in1=el_3d_no(i,1);in2=el_3d_no(i,2);in3=el_3d_no(i,3);in4=el_3d_no(i,4);
   x1=nodes(in1,1);y1=nodes(in1,2);z1=nodes(in1,3);
   x2=nodes(in2,1);y2=nodes(in2,2);z2=nodes(in2,3);
   x3=nodes(in3,1);y3=nodes(in3,2);z3=nodes(in3,3);
   x4=nodes(in4,1);y4=nodes(in4,2);z4=nodes(in4,3);
   xec(i)=(x1+x2+x3+x4)/4.;
   yec(i)=(y1+y2+y3+y4)/4.;
   zec(i)=(z1+z2+z3+z4)/4.;
   Ve(i)=1/6*(x1*y3*z2 - x1*y2*z3 + x2*y1*z3 - x2*y3*z1 - x3*y1*z2 + x3*y2*z1 + x1*y2*z4 - x1*y4*z2 - x2*y1*z4 + x2*y4*z1 + x4*y1*z2 - x4*y2*z1 - x1*y3*z4 + x1*y4*z3 + x3*y1*z4 - x3*y4*z1 - x4*y1*z3 + x4*y3*z1 + x2*y3*z4 - x2*y4*z3 - x3*y2*z4 + x3*y4*z2 + x4*y2*z3 - x4*y3*z2);

end

return;
